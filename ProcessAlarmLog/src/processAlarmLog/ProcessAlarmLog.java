package processAlarmLog;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class ProcessAlarmLog {
		//public static final String ALARM_FILE_SRC="/var/lib/jenkins/Desktop/logFiles/AlarmLog/";
		public static final String ALARM_FILE_SRC="/Users/Christina/Desktop/Log-Alarm/";
		public static void main(String[] args) {
			ProcessAlarmLog obj = new ProcessAlarmLog();
			obj.run();
		}	
		public void run() {
			while(true) {
			try{
				Thread.sleep(5000);
				}
			catch (InterruptedException e) {
					System.out.println("Interrupted Exception during sleep ...");
					continue;
				}
			File folder = new File(ALARM_FILE_SRC);
			File allFiles[] = folder.listFiles();
			File[] unprocessedFiles = new File[allFiles.length]; 
			int j = 0;
			for (int i = 0; i < allFiles.length; i++) 
			{
				if (allFiles[i].getName().contains("complete"))
					continue;
				if (allFiles[i].getName().contains("DS_Store")) {
	            	continue;
	            }				
				if (allFiles[i].isDirectory()) {
	            	continue;
	            }
				else
				{
					System.out.println(allFiles[i]);
					unprocessedFiles[j] = allFiles[i];
					j++;
				}
			}
			for (int ii = 0; ii < j; ii++) {
				String srcFileName = ALARM_FILE_SRC + unprocessedFiles[ii].getName();
				File file = new File(srcFileName);
				if(!(file.exists())) {
					System.out.println("File did not exist. Continuing");
					continue;
				}
			String csvFile = srcFileName;
			String outFileStr = "/Users/Christina/Desktop/Log-Alarm-Out/AlarmFile" + ii +".json";
			File outFile = new File(outFileStr);
			BufferedReader br = null;
			String line = "";
			String csvSplitBy = ",";
			try {
				br = new BufferedReader(new FileReader(csvFile));
				BufferedWriter writer = new BufferedWriter(new FileWriter(outFile));
	    		String sdate = "", edate = "", toolID = "", tmID = "", Airlock1 = "", Airlock2 = "", EMS1 = "", MetrologyIC = "", PM1 = "", PM2 = "", PM3 = "", PM4 = "";
	    		String AlarmID = "", AlarmName = "", PM = "";
	    		String[] metadata = new String[20];
	    		String[] metaValues = new String[20];
	    		String regex = "([0-9]{4})/([0-9]{2})/([0-9]{2})";
	    		Pattern p = Pattern.compile(regex);
	    		int readCount = 0;
    			SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy/MM/dd");
    			SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
    			SimpleDateFormat formatter3 = new SimpleDateFormat("HH:mm:ss");
    			Date date = new Date();    			
	    		while((line = br.readLine()) != null) {
	    			//System.out.println(line);
	    			readCount++;
	    			line = line.replace("'", "");
	    			if (readCount == 1)
	    			{
	    				writer.write("{");
	    				metaValues = line.split(csvSplitBy);
		    			metadata = metaValues[0].split("\\s+");
		    			System.out.println(metaValues[0]);
		    			System.out.println(metadata[0]);
		    			//
		    			date = formatter1.parse(metadata[0].trim());
		    			sdate = formatter2.format(date);
		    			metadata[1] = metadata[1].substring(0, 8);
		    			Date tdate = formatter3.parse(metadata[1]);
		    			sdate = sdate + "T" + formatter3.format(tdate);
		    			//
		    			date = formatter1.parse(metadata[2].trim());
		    			edate = formatter2.format(date);
		    			metadata[3] = metadata[3].substring(0, 8);
		    			tdate = formatter3.parse(metadata[3]);		    			
		    			edate = edate + "T" + formatter3.format(tdate);
		    			//
		    			metadata = metaValues[2].split("=");
		    			toolID = metadata[1].trim();
		    			metadata = metaValues[3].split("=");
		    			tmID = metadata[1].trim();
		    			metadata = metaValues[4].split("=");
		    			Airlock1 = metadata[1].trim();
		    			metadata = metaValues[5].split("=");
		    			Airlock2 = metadata[1].trim();
		    			metadata = metaValues[10].split("=");
		    			EMS1 = metadata[1].trim();
		    			metadata = metaValues[11].split("=");
		    			MetrologyIC = metadata[1].trim();
		    			metadata = metaValues[12].split("=");
		    			PM1 = metadata[1].trim();
		    			metadata = metaValues[13].split("=");
		    			PM2 = metadata[1].trim();
		    			metadata = metaValues[14].split("=");
		    			PM3 = metadata[1].trim();
		    			metadata = metaValues[15].split("=");
		    			PM4 = metadata[1].trim();
		    			writer.write("\"StartDate\":" + "\"" + sdate.trim() + "\"" + "," + "\"EndDate\":" + "\"" + edate.trim() + "\"" + "," + "\"ToolID\":" + "\"" + toolID.trim() + "\"" + "," + "\"tmID\":" + "\"" + tmID.trim() + "\"" + "," + "\"Airlock1ID\":" + "\"" + Airlock1.trim() + "\"" + "," + "\"Airlock2ID\":" + "\"" + Airlock2.trim() + "\"" + "," + "\"EMS1ID\":" + "\""+ EMS1 + "\"" + "," + "\"MetrologyICID\":" + "\""+ MetrologyIC + "\"" + "," + "\"PM1ID\":" + "\""+ PM1 + "\"" + "," + "\"PM2ID\":" + "\""+ PM2 + "\"," + "\"PM3ID\":" + "\""+ PM3 + "\"," + "\"PM4ID\":" + "\""+ PM4 + "\"");
		    			writer.write("}");
	    			}
	    			else
	    			{
		    			metaValues = line.split("\\s+");
	    				Matcher m = p.matcher(metaValues[0].trim());
	    				if (m.find())
	    				{
		    			writer.newLine();
		    			//
		    			date = formatter1.parse(metaValues[0].trim());
		    			sdate = formatter2.format(date);
		    			metaValues[1] = metaValues[1].substring(0, 8);
		    			Date tdate = formatter3.parse(metaValues[1]);
		    			sdate = sdate + "T" + formatter3.format(tdate);
		    			//
		    			date = formatter1.parse(metaValues[2].trim());
		    			edate = formatter2.format(date);
		    			metaValues[3] = metaValues[3].substring(0, 8);
		    			tdate = formatter3.parse(metaValues[3]);		    			
		    			edate = edate + "T" + formatter3.format(tdate);
		    			//
		    			AlarmID = metaValues[4].trim();
		    			AlarmName = metaValues[5].trim();
		    			PM = metaValues[6].trim();
		    			writer.write("{");
		    			writer.write("\"StartDate\":" + "\"" + sdate.trim() + "\"" + "," + "\"EndDate\":" + "\"" + edate.trim() + "\"" + "," + "\"AlarmID\":" + "\"" + AlarmID + "\"" + "," + "\"AlarmName\":" + "\"" + AlarmName + "\"" + "," + "\"PM\":" + "\"" + PM + "\"");
		    			writer.write("}");
	    				}
	    			}
	    		}
	    		br.close();
				writer.close();
				String oldFileName = ALARM_FILE_SRC + unprocessedFiles[ii].getName();
				String newFileName = ALARM_FILE_SRC + "complete-" + unprocessedFiles[ii].getName();
				File oldName = new File(oldFileName);
				File newName = new File(newFileName);
				if(!(oldName.renameTo(newName)))
					System.out.println("\n \n Log file was NOT renamed!!");
				System.out.println("\n \n JSON conversion is complete!!");
	    			    		
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			catch (ParseException e) {
				e.printStackTrace();
			}
			finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
			} // end for loop
		}
	}
}