package processFile;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.text.SimpleDateFormat;
import java.text.*;

public class ProcessFile {
	public static int channelSize = 0;
	//public static final String WDL_FILE_SRC = "/Users/Christina/Desktop/WDLFiles/temp1/";
	public static final String WDL_FILE_SRC = "/var/lib/jenkins/Desktop/logFiles/DataLog/";
		public static void main(String[] args) {
			ProcessFile obj = new ProcessFile();
			obj.run();
		}	
		public void run() {
			while(true) {
				try{
				Thread.sleep(5000);
				}
				catch (InterruptedException e) {
					System.out.println("Interrupted Exception during sleep ...");
					continue;
				}
				File folder = new File(WDL_FILE_SRC);
				File allFiles[] = folder.listFiles();
				File[] unprocessedFiles = new File[allFiles.length]; 
				int j = 0;
				for (int i = 0; i < allFiles.length; i++)
				{
					if (allFiles[i].getName().contains("complete"))
						continue;
					
					if (allFiles[i].getName().contains("DS_Store")) {
		            	continue;
		            }
					if (allFiles[i].isDirectory()) {
		            	continue;
		            }
					else
					{
						System.out.println(allFiles[i]);
						unprocessedFiles[j] = allFiles[i];
						j++;
					}	
				}
				
			for (int ii = 0; ii < j; ii++) {
	        String srcFileName = WDL_FILE_SRC + unprocessedFiles[ii].getName();
			File file = new File(srcFileName);
			if(!(file.exists())) {
				continue;
			}			
			String csvFile = srcFileName;
			File outMDFile = new File("/var/lib/jenkins/Desktop/logFiles/WDLMetadata.csv");
			File outDFile = new File("/var/lib/jenkins/Desktop/logFiles/WDLData.csv");
			BufferedReader br = null;
			String line = "";
			String csvSplitBy = ",";
			try {
				br = new BufferedReader(new FileReader(csvFile));
				BufferedWriter writerMD = new BufferedWriter(new FileWriter(outMDFile));
				BufferedWriter writerD = new BufferedWriter(new FileWriter(outDFile));

				int readCount = 0;
				int unitsRow = 0;
				boolean MDComplete = false;
				while ((line = br.readLine()) != null) {				
					readCount++;				
					if (readCount == unitsRow)
						continue;				
					if((!MDComplete) && ((line.replace(csvSplitBy, "")).equalsIgnoreCase("HistoricalData:")))
					{
						MDComplete = true;
						unitsRow = readCount + 2;
						continue;
					}				
					if(!MDComplete)
					{
						writerMD.write(line);
						writerMD.newLine();
					}			
					if(MDComplete)
					{
						writerD.write(line);
						writerD.newLine();
					}
				}		
				writerMD.close();
				writerD.close();
				System.out.println("\n \n Extracting metadata and data from log file ...");
				System.out.println("\n \n Extraction complete !!!");
				
				String csvFile1 = "/var/lib/jenkins/Desktop/logFiles/WDLData.csv";
				String csvFile2 = "/var/lib/jenkins/Desktop/logFiles/WDLMetadata.csv";
				String outFileStr = "/var/lib/jenkins/Desktop/flume/source/DataFile" + ii + ".json";
	    		File outFile = new File(outFileStr);
	    		String[] values = new String[channelSize];
	    		br.close();
	    		BufferedReader br1 = null;
	    		br1 = new BufferedReader(new FileReader(csvFile2));
	    		String machineID = "", waferID = "", recipe = "", PI = "", toolID = "", pmID = "";
	    		Date date = new Date();
	    		String sdate = "";
	    		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	    		String[] metadata = new String[2];
	    		String[] metaValues = new String[10];
	    		while((line = br1.readLine()) != null) {
	    			line = line.replace("\"", "");
	    			if(line.startsWith("HistoricalDataLogger file:"))
	    			{
	    				String[] dateValue = line.split("\\\\");
	    				date = formatter.parse(dateValue[5]);
	    				sdate = formatter.format(date);
	    			}
	    			if (line.startsWith("machineID"))
	    			{
		    			line = line.replace(",", "");
	    				metaValues = line.split("\\t");
	    				metadata = metaValues[0].split(":");
	    				machineID = metadata[1].trim();
	    				metadata = metaValues[1].split(":");
	    				waferID = metadata[1].trim();
	    				metadata = metaValues[2].split(":");
	    				recipe = metadata[1].trim();
	    			}
	    			if (line.startsWith("Process Interrupted"))
	    			{
		    			line = line.replace(",", "");
		    			metaValues = line.split(":");
	    				PI = metaValues[1].trim();
	    			}
	    			if(line.startsWith("Number of channels ="))
	    			{
		    			line = line.replace(",", "");
		    			metaValues = line.split("\\t");
	    				channelSize = Integer.parseInt(metaValues[1].trim());
	    			}
	    			if(line.startsWith("Tool ID:"))
	    			{
		    			line = line.replace(",", "");
		    			line = line.replace("\\t", "");
	    				metaValues = line.split(":");
	    				toolID = metaValues[1].trim();
	    			}
	    			if(line.startsWith("PM ID:"))
	    			{
		    			line = line.replace(",", "");
		    			line = line.replace("\\t", "");
	    				metaValues = line.split(":");
	    				pmID = metaValues[1].trim();
	    			}
	    			if(line.startsWith("Start:"))
	    			{
	    				metaValues = line.split(",");
	    				SimpleDateFormat formatter1 = new SimpleDateFormat("hh:mm:ss a");
	    				String temp = metaValues[1].substring(8);
	    				Date tdate = formatter1.parse(temp);
	    				SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
	    				sdate = sdate + "T" + formatter2.format(tdate);
	    			}
	    		}
	    		br1.close();
	    		String[] columnNames = new String[channelSize];
				br = new BufferedReader(new FileReader(csvFile1));
				BufferedWriter writer = new BufferedWriter(new FileWriter(outFile));
				int readCount1 = 0;
				while ((line = br.readLine()) != null) {
	    			line = line.replace(",", "");
	    			line = line.replace("\"", "");
					if(readCount1 > 1)
					{
						writer.newLine();
					}
					readCount1++;
					
					if(readCount1 == 1)
					{
				        // use comma as separator to extract the column names in the log file
						columnNames = line.split("\\s+");
					}
					else
					{
						values = line.split("\\s+");
						writer.write("{");
						writer.write("\"machineID\":" + "\"" + machineID.trim() + "\"" + "," + "\"waferId\":" + "\"" + waferID.trim() + "\"" + "," + "\"Recipe\":" + "\"" + recipe.trim() + "\"" + "," + "\"ToolID\":" + "\"" + toolID.trim() + "\"" + "," + "\"pmID\":" + "\"" + pmID.trim() + "\"" + "," + "\"Process Interrupted\":" + "\"" + PI.trim() + "\"" + "," + "\"Date\":" + "\""+ sdate + "\"");
						float n = 0;
						for(int i=0; i<channelSize; i++)
						{
							values[i] = values[i].trim();
							try {
							n = Float.parseFloat(values[i]);
							writer.write("," + "\"" + columnNames[i] + "\":" + n );
							}
							 
							catch (NumberFormatException e)
							{
								continue;
							}
						}
						writer.write("}");
					}				
				}
				writer.close();
				String oldFileName = WDL_FILE_SRC + unprocessedFiles[ii].getName();
				String newFileName = WDL_FILE_SRC + "complete-" + unprocessedFiles[ii].getName();
				File oldName = new File(oldFileName);
				File newName = new File(newFileName);
				if(!(oldName.renameTo(newName)))
					System.out.println("\n \n Log file was NOT renamed!!");
				System.out.println("\n \n JSON conversion is complete!!");
	    		
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} 
			catch (ParseException e) {
				e.printStackTrace();
			}
			finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
			} // For loop with all files
			}
		}
}