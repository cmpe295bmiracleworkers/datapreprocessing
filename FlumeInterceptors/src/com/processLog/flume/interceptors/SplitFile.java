package com.example.flume.interceptors;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.interceptor.Interceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * This is a sample interceptor, which appends Host:Time entries to the header
 * field HostTime
 */
public class SplitFile extends AbstractFlumeInterceptor {

    public static final Logger LOGGER = LoggerFactory.getLogger(HostTimeInterceptor.class);
    public static String[] columnNames = new String[233];
    /*private final String header;
    private String host;
    public static final String DEFAULT_SEPARATOR = ",";
    public static final String DEFAULT_KEYVAL_SEPARATOR = ":";
*/

    /**
     * Default constructor. Private constructor to avoid being build by outside source
     *
     * @param headerKey Key for the header to be used
     */
    private SplitFile() {
        // NOOP
    }

    @Override
    public void initialize() {
        // NOOP
    }

    @Override
    public Event intercept(Event event) {
        Map<String, String> headers = event.getHeaders();

/*        String headerValue = headers.get(header);
        if(headerValue != null) {
            headerValue += DEFAULT_SEPARATOR;
        } else {
            headerValue = "";
        }
        headerValue += host + DEFAULT_KEYVAL_SEPARATOR + System.currentTimeMillis();
        headers.put(header, headerValue);
        
        System.out.println("\n\n Interceptor is called !!!\n\n");
*/
        String csvFile = "/Users/Christina/Downloads/apache-flume-1.6.0-bin/source/WDL-FullFile.csv"; //sampleData.csv";
		File outMDFile = new File("/Users/Christina/Desktop/WDLMetadata.csv");
		File outDFile = new File("/Users/Christina/Desktop/WDLData.csv");
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		System.out.println("Splitting metadata and data ... ");
		try {

			br = new BufferedReader(new FileReader(csvFile));
			BufferedWriter writerMD = new BufferedWriter(new FileWriter(outMDFile));
			BufferedWriter writerD = new BufferedWriter(new FileWriter(outDFile));

			int readCount = 0;
			int unitsRow = 0;
			boolean MDComplete = false;
			while ((line = br.readLine()) != null) {				
				readCount++;				
				if (readCount == unitsRow)
					continue;				
				if((!MDComplete) && ((line.replace(cvsSplitBy, "")).equalsIgnoreCase("HistoricalData:")))
				{
					MDComplete = true;
					unitsRow = readCount + 2;
					//System.out.println("readCount = "+ readCount + "\n");
					continue;
				}				
				if(!MDComplete)
				{
					writerMD.write(line);
					writerMD.newLine();
				}			
				if(MDComplete)
				{
					writerD.write(line);
					writerD.newLine();
				}
			}		
			writerMD.close();
			writerD.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("\n \n Splitting complete!!");
        return event;
    }

    @Override
    public void close() {
        // NOOP
    }

    public static class Builder implements Interceptor.Builder {

        //private String headerkey = "HostTime";

        @Override
        public Interceptor build() {
            return new SplitFile();
        }

        @Override
        public void configure(Context context) {
            //headerkey = context.getString("key");
        }
    }
}