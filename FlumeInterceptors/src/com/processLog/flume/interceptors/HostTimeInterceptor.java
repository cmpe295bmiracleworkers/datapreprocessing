package com.example.flume.interceptors;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.interceptor.Interceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * This is a sample interceptor, which appends Host:Time entries to the header
 * field HostTime
 */
public class HostTimeInterceptor extends AbstractFlumeInterceptor {

    public static final Logger LOGGER = LoggerFactory.getLogger(HostTimeInterceptor.class);
    public static String[] columnNames = new String[233];
    /*private final String header;
    private String host;
    public static final String DEFAULT_SEPARATOR = ",";
    public static final String DEFAULT_KEYVAL_SEPARATOR = ":";
*/

    /**
     * Default constructor. Private constructor to avoid being build by outside source
     *
     * @param headerKey Key for the header to be used
     */
    private HostTimeInterceptor() {
        // NOOP
    }

    @Override
    public void initialize() {
        // NOOP
    }

    @Override
    public Event intercept(Event event) {
        Map<String, String> headers = event.getHeaders();

/*        String headerValue = headers.get(header);
        if(headerValue != null) {
            headerValue += DEFAULT_SEPARATOR;
        } else {
            headerValue = "";
        }
        headerValue += host + DEFAULT_KEYVAL_SEPARATOR + System.currentTimeMillis();
        headers.put(header, headerValue);
        
        System.out.println("\n\n Interceptor is called !!!\n\n");
*/
    		String csvFile = "/Users/Christina/Desktop/WDLData.csv";
    		File outFile = new File("/Users/Christina/Desktop/WDLDataOFile.json");
    		BufferedReader br = null;
    		String line = "";
    		String cvsSplitBy = ",";
    		System.out.println("Processing log data ... ");
    		try {
    			String[] values = new String[233];
    			br = new BufferedReader(new FileReader(csvFile));
    			BufferedWriter writer = new BufferedWriter(new FileWriter(outFile));
    			int readCount = 0;
    			while ((line = br.readLine()) != null) {
    				if(readCount > 1)
    				{
    					writer.newLine();
    				}
    				readCount++;
    				
    				if(readCount == 1)
    				{
    			        // use comma as separator to extract the column names in the log file
    					columnNames = line.split(cvsSplitBy);
    				}
    				else
    				{
    					values = line.split(cvsSplitBy);
    					//System.out.println("");
    					writer.write("{");
    					float n = 0;
    					for(int i=0; i<233; i++)
    					{
    						values[i] = values[i].trim();
    						try {
    						n = Float.parseFloat(values[i]);
    						if(i == 232)
    							writer.write("\"" + columnNames[i] + "\":" + n );
    						else
    							writer.write("\"" + columnNames[i] + "\":" + n + ",");
    						}
    						catch (NumberFormatException e)
    						{
    							continue;
    							}
    					}
    					writer.write("}");
    				}				
    			}
    			writer.close();
    		} catch (FileNotFoundException e) {
    			e.printStackTrace();
    		} catch (IOException e) {
    			e.printStackTrace();
    		} finally {
    			if (br != null) {
    				try {
    					br.close();
    				} catch (IOException e) {
    					e.printStackTrace();
    				}
    			}
    		}
    		System.out.println("\n \n ******* Converted WDL file to JSON !! *******\n\n");
        return event;
    }

    @Override
    public void close() {
        // NOOP
    }

    public static class Builder implements Interceptor.Builder {

        //private String headerkey = "HostTime";

        @Override
        public Interceptor build() {
            return new HostTimeInterceptor();
        }

        @Override
        public void configure(Context context) {
            //headerkey = context.getString("key");
        }
    }
}