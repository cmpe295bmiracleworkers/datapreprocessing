package processWDL;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.text.SimpleDateFormat;
import java.text.*;

public class ProcessWDL {
	public static final String WDL_FILES_SRC = "/Users/Christina/Desktop/WDLFiles/";
	public static final String WDL_FILES_OUTPUT = "/Users/Christina/Desktop/WDLFiles/Output/";
	public static int channelSize = 0;	
    //public static String[] columnNames = new String[channelSize];
		public static void main(String[] args) {
			ProcessWDL obj = new ProcessWDL();
			obj.run();
		}	
		public void run() {
			
			
			while(true) {
			//String csvFile = "/var/lib/jenkins/MasterProject/logFiles/WDL-FullFile.csv"; //sampleData.csv";
			/*
			 * String pattern = "WDL?*.csv";
			 * 
			 */
				//if (bulkFileProcess) {
					final File folder = new File(WDL_FILES_SRC);
					for (final File fileEntry : folder.listFiles()) {
				        
				            //System.out.println(fileEntry.getName());
				            if (fileEntry.getName().contains("complete")) {
				            	continue;
				            }
				            
				            if (fileEntry.getName().contains("DS_Store")) {
				            	continue;
				            }
				            
				            if (fileEntry.isDirectory()) {
				            	continue;
				            }			
				            
				            fileEntry.getName().replace(".", "_");
				            // First rename the files to .csv
				            //String csvFileStr = WDL_FILES_SRC + "WDLFile" + ".csv";
				            //File oldName = new File(WDL_FILES_SRC + fileEntry.getName());
							File csvFile = fileEntry;
							//oldName.renameTo(csvFile);
							//System.exit(0);
							System.out.println(fileEntry.getName());
							if(!(csvFile.exists()))
								continue;
							String outMDFileStr = WDL_FILES_OUTPUT + "WDLMetadata.csv";
							File outMDFile = new File(outMDFileStr);
							
							String outDFileStr = WDL_FILES_OUTPUT  + "WDLData.csv";
							File outDFile = new File(outDFileStr);
							
							System.out.println(outMDFileStr);
							System.exit(0);
				   // }
					//File file = new File("/Users/Christina/Desktop/wdl6.csv");
					//if(!(file.exists()))
					//	continue;
					//String csvFile = "/Users/Christina/Desktop/wdl6.csv"; //sampleData.csv";
					//File outMDFile = new File("/Users/Christina/Desktop/WDLMetadata.csv");
					//File outDFile = new File("/Users/Christina/Desktop/WDLData.csv");
					BufferedReader br = null;
					String line = "";
					String csvSplitBy = ",";
					try {
						br = new BufferedReader(new FileReader(csvFile));
						BufferedWriter writerMD = new BufferedWriter(new FileWriter(outMDFile));
						BufferedWriter writerD = new BufferedWriter(new FileWriter(outDFile));

						int readCount = 0;
						int unitsRow = 0;
						boolean MDComplete = false;
						while ((line = br.readLine()) != null) {				
							readCount++;				
							if (readCount == unitsRow)
								continue;				
							if((!MDComplete) && ((line.replace(csvSplitBy, "")).equalsIgnoreCase("HistoricalData:")))
							{
								MDComplete = true;
								unitsRow = readCount + 2;
								//System.out.println("readCount = "+ readCount + "\n");
								continue;
							}				
							if(!MDComplete)
							{
								writerMD.write(line);
								writerMD.newLine();
							}			
							if(MDComplete)
							{
								writerD.write(line);
								writerD.newLine();
							}
						}		
						writerMD.close();
						writerD.close();
						System.out.println("\n \n Extracting metadata and data from log file ...");
						System.out.println("\n \n Extraction complete !!!");
						
						//String csvFile1 = "/Users/Christina/Desktop/WDLData.csv";
						//String csvFile2 = "/Users/Christina/Desktop/WDLMetadata.csv";
						String csvFile1 = outMDFileStr;
						String csvFile2 = outDFileStr;
						String outFileStr = WDL_FILES_OUTPUT + fileEntry.getName() + ".json";
						File outFile = new File(outFileStr);
			    		//File outFile = new File("/Users/Christina/Downloads/apache-flume-1.6.0-bin/source/WDLDataOFile51.json");
			    		String[] values = new String[channelSize];
			    		br.close();
			    		BufferedReader br1 = null;
			    		br1 = new BufferedReader(new FileReader(csvFile2));
			    		String machineID = "", waferID = "", recipe = "", PI = "", toolID = "", pmID = "";
			    		Date date = new Date();
			    		String sdate = "";
			    		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			    		String[] metadata = new String[2];
			    		String[] metaValues = new String[10];
			    		while((line = br1.readLine()) != null) {
			    			line = line.replace("\"", "");
			    			if(line.startsWith("HistoricalDataLogger file:"))
			    			{
			    				String[] dateValue = line.split("\\\\");
			    				date = formatter.parse(dateValue[5]);
			    				sdate = formatter.format(date);
			    			}
			    			if (line.startsWith("machineID"))
			    			{
				    			line = line.replace(",", "");
			    				metaValues = line.split("\\t");
			    				metadata = metaValues[0].split(":");
			    				machineID = metadata[1].trim();
			    				metadata = metaValues[1].split(":");
			    				waferID = metadata[1].trim();
			    				metadata = metaValues[2].split(":");
			    				recipe = metadata[1].trim();
			    			}
			    			if (line.startsWith("Process Interrupted"))
			    			{
				    			line = line.replace(",", "");
				    			metaValues = line.split(":");
			    				PI = metaValues[1].trim();
			    			}
			    			if(line.startsWith("Number of channels ="))
			    			{
				    			line = line.replace(",", "");
				    			metaValues = line.split("\\t");
				    			System.out.println("metaValues[1]: " + metaValues[1]);
			    				//String[] noOfChannels = metaValues[0].split("=");
			    				//System.out.println("noOfChannels[2]: " + noOfChannels[2]);
			    				//noOfChannels[2] = noOfChannels[2].trim();
			    				channelSize = Integer.parseInt(metaValues[1].trim());
			    			}
			    			if(line.startsWith("Tool ID:"))
			    			{
				    			line = line.replace(",", "");
				    			line = line.replace("\\t", "");
			    				metaValues = line.split(":");
			    				toolID = metaValues[1].trim();
			    			}
			    			if(line.startsWith("PM ID:"))
			    			{
				    			line = line.replace(",", "");
				    			line = line.replace("\\t", "");
			    				metaValues = line.split(":");
			    				pmID = metaValues[1].trim();
			    			}
			    			if(line.startsWith("Start:"))
			    			{
			    				metaValues = line.split(",");
			    				SimpleDateFormat formatter1 = new SimpleDateFormat("hh:mm:ss a");
			    				String temp = metaValues[1].substring(8);
			    				Date tdate = formatter1.parse(temp);
			    				SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
			    				sdate = sdate + "T" + formatter2.format(tdate);
			    			}
			    		}
			    		br1.close();
			    		String[] columnNames = new String[channelSize];
						br = new BufferedReader(new FileReader(csvFile1));
						BufferedWriter writer = new BufferedWriter(new FileWriter(outFile));
						int readCount1 = 0;
						while ((line = br.readLine()) != null) {
			    			line = line.replace(",", "");
			    			line = line.replace("\"", "");
							if(readCount1 > 1)
							{
								writer.newLine();
							}
							readCount1++;
							
							if(readCount1 == 1)
							{
						        // use comma as separator to extract the column names in the log file
								columnNames = line.split("\\s+");
							}
							else
							{
								values = line.split("\\s+");
								//boolean first = true;
								writer.write("{");
								writer.write("\"machineID\":" + "\"" + machineID.trim() + "\"" + "," + "\"waferId\":" + "\"" + waferID.trim() + "\"" + "," + "\"Recipe\":" + "\"" + recipe.trim() + "\"" + "," + "\"ToolID\":" + "\"" + toolID.trim() + "\"" + "," + "\"pmID\":" + "\"" + pmID.trim() + "\"" + "," + "\"Process Interrupted\":" + "\"" + PI.trim() + "\"" + "," + "\"Date\":" + "\""+ sdate + "\"");
								float n = 0;
								for(int i=0; i<channelSize; i++)
								{
									values[i] = values[i].trim();
									try {
									n = Float.parseFloat(values[i]);
									/*
									if(i == (channelSize - 1))
										writer.write("\"" + columnNames[i] + "\":" + n );
									else
										writer.write("\"" + columnNames[i] + "\":" + n + ",");
									*/
									writer.write("," + "\"" + columnNames[i] + "\":" + n );
									 
									/* 
									 * if (first)
									{
										writer.write("\"" + columnNames[i] + "\":" + n );
										first = false;
									}
									else
										writer.write("," + "\"" + columnNames[i] + "\":" + n );
										*/
									}
									 
									catch (NumberFormatException e)
									{
										continue;
									}
								}
								writer.write("}");
							}				
						}
						writer.close();
						//File oldName = new File("/Users/Christina/Desktop/wdl6.csv");
						//File newName = new File("/Users/Christina/Desktop/wdl6-complete.csv");
						String csvFileCompleteStr = WDL_FILES_SRC + fileEntry.getName() + "-complete.csv";
						File csvFileComplete = new File(csvFileCompleteStr);
						//if(!(oldName.renameTo(newName)))
						if (!(csvFile.renameTo(csvFileComplete)))
							System.out.println("\n \n Log file was NOT renamed!!");
						System.out.println("\n \n JSON conversion is complete!!");
			    		
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} 
					catch (ParseException e) {
						e.printStackTrace();
					}
					finally {
						if (br != null) {
							try {
								br.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
					
				}
			}

		}
	}



				
			

